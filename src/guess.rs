//use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Hello, world!");
    println!("Input a number!");

    //let mut guess = String::new();
    let secret_number = rand::thread_rng().gen_range(1..101);
    
    loop {
      let mut guess = String::new();
      std::io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

      let guess: u32 = guess.trim().parse().expect("none input exception");

      //println!("The answer!: {}", secret_number);
      println!("Your input: {}", guess);
    
      match guess.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal =>  { 
          println!("You win!");
          break;
        },
      }
    }
}
