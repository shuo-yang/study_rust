fn main() {
    let mut s1 = String::from("hello");

    let len  = calculate_length(&s1);

    push(&mut s1);

    println!("The old length of '{}' is {}.", s1, len);

    let a = [1, 2, 3, 4, 5];
    let slice = &a[..2];

    assert_eq!(slice, &[1, 2]);
}

fn calculate_length(s: &String)-> usize  {
    let length = s.len(); // len() returns the length of a String

    length
}

fn push(s: &mut String) {
    s.push_str("appended")
}
