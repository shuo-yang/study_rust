fn main() {
    let mut y = 5;
    println!("The value is {} " , y);
    y = 4;
    println!("The value is {} ", y);

    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    println!("c{} Z{} cat{} ", c, z, heart_eyed_cat);
    
    let tup: (i32, f64, u8 ) = (500, 6.4, 1);
    println!("tuple[0] {} ", tup.0);

    let array: [i32; 5] = [1, 2, 3, 4, 5];
    println!("array[1] {} ", array[1]);

    //let mut inp = String::new();

    //std::io::stdin().read_line(&mut inp).expect("please input");

    //let a:i32 = inp.trim().parse().expect("parse error");
    //println!("input is {} ", a);

    let mut x = 5;
    let y: i32 = {
       let x = 6;
       x * 2
    };

    let z = loop {
      x = x + 1;
      if x == 8 {
         break x * 2;
      }
    };

    println!("x is {} y is {} z is {}", x, y, z);
}
